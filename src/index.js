import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    return (
      <button
        className={props.winning}
        onClick={props.onClick}
      >
        {props.value}
      </button>
    );
}

class Board extends React.Component {
  renderSquare(i) {
    let winning = false;
    let w;
    for (w = 0; w < this.props.winning.length ; w++) {
      if ( i === this.props.winning[w] ) {
        winning = true;
      }
    }
    const winStyle = ( winning ) ? "square winning" : "square" ;
    return (
        <Square
          key={'square-'+i}
          value={this.props.squares[i]}
          onClick={() => this.props.onClick(i)}
          winning={winStyle}
        />
      );
  }

  renderRow(r) {
    const grid = this.props.grid;
    const cols=[];
    let c, rc;
    for (c = 1; c <= grid; c++) {
      rc = (r * 10) + c;
      cols.push(
        <div>
          {this.renderSquare(rc)}
        </div>
      );
    }
    return (
      <div className="board-row">
        {cols}
      </div>
    )
  }

  render() {
    const grid = this.props.grid;
    const rows=[];
    let r;
    for (r = 1; r <= grid; r++) {
      rows.push(
        <div>
        {this.renderRow(r)}
        </div>
      );
    }
    return (
      <div>
        {rows}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(99).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
      grid: 3,
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleClick(i) {
    //alert(i);
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    // on fait une copie grâce à slice()
    const squares = current.squares.slice();
    if (calculateWinner(squares, this.state.grid) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }
  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }
  handleChange(ev) {
    //alert(ev.target.value);
    this.setState({
      history: [{
        squares: Array(99).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
      grid: ev.target.value,
    });
  }
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares, this.state.grid);

    const moves = history.map((step,move) => {
      const desc = move ? 'Go to move #' + move : 'Go to start';
      //alert(move);
      let stepStyle ;
      if ( move < this.state.stepNumber ) {
        stepStyle = "before";
      } else if ( move == this.state.stepNumber ) {
        stepStyle = "now"
      } else {
        stepStyle = "after"
      }
      return (
        <li key={move}>
          <button className={stepStyle} onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    let winning;
    if (winner) {
      status = 'Winner is ' + winner[0];
      winning = winner[1];
    } else {
      status = 'Next player is ' + (this.state.xIsNext ? 'X' : 'O');
      winning = [];
    }

    return (
     <div> 
	  <div className="intro">
		<p>ReactJS TicTacToe game<br />
		done from {"this"} <a href="https://fr.reactjs.org/tutorial/tutorial.html#wrapping-up" target="_blank" >tutorial</a><br />
		but improved by my code refactoring :</p>
		<ul>
			<li>{"Bold the currently selected item in the move list."}</li>
			<li>{"Rewrite Board to use two loops to make the squares instead of hardcoding them."}</li>
			<li>{"When someone wins, highlight the squares that caused the win."}</li>
			<li>{"Let you choose how many squares you want: from 2x2 to 9x9."}</li>
			<li>{"Which means to fully rewrite the helper function to check if someone won."}</li>
		</ul>
	  </div>
	  
	  <div>
          <Select
            grid={this.state.grid}
            onChange={(e) => this.handleChange(e)}
          />
      </div>
		
	  <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
            grid={this.state.grid}
            winning={winning}
          />
        </div>
	  </div>
	  
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      
	 </div>
    );
  }
}

class Select extends React.Component {
  render() {
    const selects=[];
    let s
    for (s = 2; s <= 9; s++) {
      selects.push(
        <option value={s}>{s}</option>
      );
    }
    return (
      <select value={this.props.grid} onChange={(e) => this.props.onChange(e)}>
        {selects}
      </select>
    )
  }
}

// helper function
function calculateWinner(squares, grid) {
  const lines = [];
  let element = [];
  // horizontal winning series
  for (let r = 1; r <= grid; r++) {
    element = [];
    for (let c = 1; c <= grid; c++) {
      element.push((r*10)+c);
    }
    lines.push(element);
  }
  // vertical winning series
  for (let c = 1; c <= grid; c++) {
    element = [];
    for (let r = 1; r <= grid; r++) {
      element.push((r*10)+c);
    }
    lines.push(element);
  }
  // 2 diagonal winning series
  element = [];
  for (let g = 1; g <= grid; g++) {
    element.push((g*10)+g);
  }
  lines.push(element);
  element = [];
  for (let g = 1; g <= grid; g++) {
    element.push((g*10)+(1 + grid - g));
  }
  lines.push(element);
  // checking
  var win;
  for (let i = 0; i < lines.length; i++) {
    win = squares[lines[i][0]];
    //alert(win);
    for (let j = 0; j < lines[i].length; j++) {
      if (squares[lines[i][0]] === null || squares[lines[i][0]] !== squares[lines[i][j]]) {
        win = null;
      }
    }
    if ( win !== null ) { return [win,lines[i]] }
  }
  return null
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
